﻿using Consul;
using Ocelot.Logging;
using Ocelot.Provider.Consul;
using Ocelot.Provider.Consul.Interfaces;

namespace Common.CustomLoadBalancer
{
    /// <summary>
    /// 解决服务发现拿到的是consul的node问题，改成获取ip
    /// </summary>
    public class ConsulServiceBuilder : DefaultConsulServiceBuilder
    {
        public ConsulServiceBuilder(Func<ConsulRegistryConfiguration> configurationFactory, IConsulClientFactory clientFactory, IOcelotLoggerFactory loggerFactory)
        : base(configurationFactory, clientFactory, loggerFactory) { }
        // I want to use the agent service IP address as the downstream hostname
        protected override string GetDownstreamHost(ServiceEntry entry, Node node) => entry.Service.Address;
    }
}
