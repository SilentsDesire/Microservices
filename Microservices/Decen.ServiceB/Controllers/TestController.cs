using Common.CAP;
using Common.Jwt;
using Common.MefHelp;
using Common.MinIo;
using Common.MinIo.Model;
using Common.SelectService;
using Common.Self;
using Common.Tools;
using Common.Tools.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Decen.ServiceB.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private readonly ILogger<TestController> _logger;
        //private readonly OrderService _orderService;
        public TestController(ILogger<TestController> logger)
        {
            _logger = logger;
            //_orderService = orderService;
        }

        [HttpGet]
        public ResponseResult<string> GetIp()
        {
            return ResponseResult<string>.Success($"{SelfConfig.Instance.Ip}:{SelfConfig.Instance.Port}");
        }
        [HttpGet]
        public ResponseResult<string> GetMsg()
        {
            return ResponseResult<string>.Success($"{SelfConfig.Instance.Ip}:{SelfConfig.Instance.Port} 这是ServiceB的信息");
        }
        [HttpGet]
        public async Task<ResponseResult<int>> HandleStationNum(int num)
        {
            var self = Mef.GetService<SelfPush>();
            var stationNum = self.HandleStationNum(num);
            await self.UpdateCache();
            return ResponseResult<int>.Success(stationNum);
        }
        [HttpGet]
        public async Task<ResponseResult<string>> GetServiceA()
        {
            var result = await ClientService.GetAsync<string>("ServiceA", $"/Test/GetMsg");
            return result;
        }
        /// <summary>
        /// 查询文件
        /// </summary>
        /// <param name="path">文件路径</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseResult<List<BucketFileInfo>>> SearchObjectsAsync(string? path = null)
        {
            var result = await Mef.GetService<IStorageObjectOperate>().SearchObjectsAsync(path);
            return ResponseResult<List<BucketFileInfo>>.Success(result);
        }
        /// <summary>
        /// 获取所有存储桶大小
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseResult<List<BucketSize>>> GetBucketSizeAsync()
        {
            var result = await Mef.GetService<IStorageObjectOperate>().GetBucketSizeAsync();
            return ResponseResult<List<BucketSize>>.Success(result);
        }
        ///// <summary>
        ///// 测试Cap
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //public async Task<ResponseResult<bool>> GetCapAsync()
        //{
        //    await _orderService.CreateOrderAsync(new OrderCreatedEvent
        //    {
        //        CustomerName = "",
        //        OrderDate = DateTime.Now,
        //        OrderId = 11
        //    });
        //    return ResponseResult<bool>.Success(true);
        //}
        [HttpGet]
        [AllowAnonymous]
        public string GetJwtToken()
        {
            var token = JwtHelper.GetJwtToken(new JwtUserModel()
            {
                UserName = "张三",
                UserId = "32",
                UserType = "admin"
            });
            return token;
        }
    }
}
