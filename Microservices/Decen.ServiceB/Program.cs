using Autofac;
using Common.Consul.Extensions;
using Common.ServiceDiscovery.Extensions;
using Common.Cache;
using Common.MefHelp;
using Common.Self;
using SkyApm.Utilities.DependencyInjection;
using Common.Tools.Filter;
using Common.SelectService.Extensions;
using Common.Tools.Model;
using System.Runtime.InteropServices;
using Common.Tools;
using Common.MinIo.Extensions;
using Common.Cap.Extensions;
using Common.Jwt.Extensions;
namespace Decen.ServiceB
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            // 加载配置文件
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var isUseSkywalking = configuration.GetValue<bool>("IsUseSkywalking");
            if (isUseSkywalking) AddSkyWalkingEnvironment();//添加skywalking环境变量，必须放在builder前面

            var builder = WebApplication.CreateBuilder(args);
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerAddJwtHeader();
            });
            #region 自定义
            //判断当前系统是否为windows
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                builder.Host.UseWindowsService();//加上这句，可以做成windows服务
            }
            builder.Services.AddControllers(options =>
            {
                options.Filters.Add<GlobalActionFilterAttribute>();
                options.Filters.Add<ExceptionFilter>();
            });
            // Add services to the container. 
            builder.AddSelfHost();//添加自身ip和端口
            if (isUseSkywalking) builder.Services.AddSkyApmExtensions(); //添加Skywalking相关配置 
            builder.Services.AddServiceDiscovery(builder.Configuration, option => { option.UseConsul(); });//注入服务注册和发现 (遇到本地调试心跳检测不过的，需要把launchSettings.json的localhost改成*)
            builder.Services.AddSelectService();//添加选择服务
            builder.Services.AddCustomCache(); //注入缓存  
           //builder.Services.AddMinIoConfig(); //注入MinIo
           //builder.Services.AddCapConfig();//注入Cap
            builder.Services.AddJwtService();//注入jwt服务
            var container = new ContainerBuilder().AddMefContainer(builder.Services)
               //.AddByFilterPattern("Common.*.dll")
               //.AddRpc(builder.Services, builder.Configuration, rpcbuilder =>
               //{
               //    rpcbuilder.AddClient();
               //    rpcbuilder.AddServer();
               //    rpcbuilder.AddClientGlobalInterceptor<ClientGlobalInterceptor>();
               //    rpcbuilder.AddServerGlobalInterceptor<ServerGlobalInterceptor>();
               //    rpcbuilder.AddCircuitBreaker<CircuitBreakerEvent>();
               //    rpcbuilder.AddOption(RpcOptions.Remoting_Invoke_CancellationTokenSource_Timeout, TimeSpan.FromSeconds(60));
               //    rpcbuilder.AddOptions();
               //})
               .Completed();//使用autofac 
            try
            {
                Mef.GetService<SelfPush>().Start();//启动自身服务信息推送到redis
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
            #endregion

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            // 使用服务注册
            app.UseConsulRegisterService(builder.Configuration);

            //app.UseMiddleware<JwtCheckMiddleware>();
            //配置跨域
            app.UseCors(options =>
            {
                options.AllowAnyHeader();
                options.AllowAnyMethod();
                options.AllowAnyOrigin();
            });
            app.UseAuthentication();//要在授权之前认证，这个和[Authorize]特性有关
            app.UseAuthorization();

            app.MapControllers();
            app.Run();
        }
        /// <summary>
        /// 添加skywalking环境变量，必须放在builder前面
        /// </summary>
        public static void AddSkyWalkingEnvironment()
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("skyapm.json").Build();
            var serviceName = configuration.GetValue<string>("SkyWalking:ServiceName");
            Environment.SetEnvironmentVariable("ASPNETCORE_HOSTINGSTARTUPASSEMBLIES", "SkyAPM.Agent.AspNetCore");
            Environment.SetEnvironmentVariable("SKYWALKING__SERVICENAME", serviceName);
            LogHelper.Info($"加载skywalking环境变量");
        }
        public static void AddSelfHost(this WebApplicationBuilder builder)
        {
            // 加载配置文件
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var ip = configuration.GetValue<string>("ServiceDiscovery:SelfHost");
            var port = configuration.GetValue<int>("ServiceDiscovery:SelfPort");
            var serviceName = configuration.GetValue<string>("ServiceDiscovery:ServiceName");
            SelfConfig.SetInstance(ip ?? string.Empty, port, serviceName);
            builder.WebHost.UseUrls(new[] { $"http://*:{port}" });
            LogHelper.Info($"初始化服务...端口为:{port}");
        }
    }
}
