﻿using Common.MinIo.Model;

namespace Common.MinIo
{
    /// <summary>
    /// 文件操作相关接口
    /// </summary>
    public interface IStorageObjectOperate
    {
        /// <summary>
        /// 获取存储地址
        /// </summary>
        /// <returns></returns>
        Task<string> GetStorageUrl();
        /// <summary>
        /// 上传对象
        /// </summary>
        /// <param name="objectType">文件类型</param>
        /// <param name="objName">文件唯一Id</param>
        /// <param name="data">文件数据</param>
        /// <returns></returns>
        Task<bool> PutObjectAsync(StorageObjectTypeDefine objectType, string objName, Stream data);

        /// <summary>
        /// 上传对象
        /// </summary>
        /// <param name="objectType">文件类型</param>
        /// <param name="objName">文件唯一Id</param>
        /// <param name="filePath">文件路径</param>
        /// <returns></returns>
        Task<bool> PutObjectAsync(StorageObjectTypeDefine objectType, string objName, string filePath);
        /// <summary>
        /// 上传对象
        /// </summary>
        /// <param name="bucketName">存储桶名称</param>
        /// <param name="objName">文件唯一Id</param>
        /// <param name="data">文件数据</param>
        /// <returns></returns>
        Task<bool> PutObjectAsync(string bucketName, string objName, Stream data);
        /// <summary>
        /// 上传对象
        /// </summary>
        /// <param name="bucketName">存储桶名称</param>
        /// <param name="objName">文件唯一Id</param>
        /// <param name="filePath">文件路径</param>
        /// <returns></returns>
        Task<bool> PutObjectAsync(string bucketName, string objName, string filePath);

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <param name="objectType">文件类型</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <param name="offset">是起始字节的位置</param>
        /// <param name="length">读取长度</param>
        /// <returns></returns>
        Task<Stream> GetObjectAsync(StorageObjectTypeDefine objectType, string objectName, long offset = 0, long length = 0);
        /// <summary>
        /// 获取对象
        /// </summary>
        /// <param name="bucketName">存储桶名称</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <param name="offset">是起始字节的位置</param>
        /// <param name="length">读取长度</param>
        /// <returns></returns>
        Task<Stream> GetObjectAsync(string bucketName, string objectName, long offset = 0, long length = 0);
        /// <summary>
        /// 获取文件大小
        /// </summary>
        /// <param name="objectType">文件类型</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <returns></returns>
        Task<long> GetObjectSize(StorageObjectTypeDefine objectType, string objectName);
        /// <summary>
        /// 获取文件大小
        /// </summary>
        /// <param name="bucketName">存储桶名称</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <returns></returns>
        Task<long> GetObjectSize(string bucketName, string objectName);
        /// <summary>
        /// 删除对象
        /// </summary>
        /// <param name="objectType">文件类型</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <returns></returns>
        Task RemoveObjectAsync(StorageObjectTypeDefine objectType, string objectName);
        /// <summary>
        /// 删除对象
        /// </summary>
        /// <param name="bucketName">存储桶名称</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <returns></returns>
        Task RemoveObjectAsync(string bucketName, string objectName);
        /// <summary>
        /// 批量删除对象
        /// </summary>
        /// <param name="objectType">文件类型</param>
        /// <param name="objectNames">文件唯一Id</param> 
        /// <returns></returns>
        Task RemoveObjectAsync(StorageObjectTypeDefine objectType, IList<string> objectNames);
        /// <summary>
        /// 批量删除对象
        /// </summary>
        /// <param name="bucketName">存储桶名称</param>
        /// <param name="objectNames">文件唯一Id</param> 
        /// <returns></returns>
        Task RemoveObjectAsync(string bucketName, IList<string> objectNames);
        /// <summary>
        /// 查询存储桶文件
        /// </summary>
        /// <param name="path">文件路径</param>
        /// <param name="isRecursive">是否递归</param>
        /// <returns>返回当前目录下的文件或者文件夹</returns>
        Task<List<BucketFileInfo>> SearchObjectsAsync(string path, bool isRecursive = false);
        /// <summary>
        /// 获取存储桶的大小
        /// </summary>
        /// <returns>返回当前目录下的文件或者文件夹</returns>
        Task<List<BucketSize>> GetBucketSizeAsync();
        /// <summary>
        /// 按条件删除文件
        /// </summary>
        /// <returns>返回当前目录下的文件或者文件夹</returns>
        Task<bool> DeleteObjectsByConditionAsync(DeleteParam param);
        /// <summary>
        /// 服务端拷贝对象
        /// </summary> 
        /// <param name="objType">源文件类型</param>
        /// <param name="objectName">源文件唯一Id</param>
        /// <param name="destType">目标文件类型</param>
        /// <param name="destObjName">目标文件唯一Id</param>
        /// <returns></returns>
        Task CopyObjectAsync(StorageObjectTypeDefine objType, string objectName, StorageObjectTypeDefine destType, string destObjName);
        /// <summary>
        /// 服务端拷贝对象
        /// </summary> 
        /// <param name="bucketName">源存储桶</param>
        /// <param name="objectName">源文件唯一Id</param>
        /// <param name="destBucketName">目标存储桶</param>
        /// <param name="destObjName">目标文件唯一Id</param>
        /// <returns></returns>
        Task CopyObjectAsync(string bucketName, string objectName, string destBucketName, string destObjName);
        /// <summary>
        /// 生成下载url
        /// </summary>
        /// <param name="objType">文件类型</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <param name="expiresSecond">过期时间</param> 
        /// <returns></returns>
        Task<string> PresignedGetObjectAsync(StorageObjectTypeDefine objType, string objectName, int expiresSecond = 60);
        /// <summary>
        /// 生成下载url
        /// </summary>
        /// <param name="bucketName">存储桶名称</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <param name="expiresSecond">过期时间</param> 
        /// <returns></returns>
        Task<string> PresignedGetObjectAsync(string bucketName, string objectName, int expiresSecond = 60);
        /// <summary>
        /// 生成上传url
        /// </summary>
        /// <param name="objType">文件类型</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <param name="expiresSecond">过期时间</param> 
        /// <returns></returns>
        Task<string> PresignedPutObjectAsync(StorageObjectTypeDefine objType, string objectName, int expiresSecond = 60);
        /// <summary>
        /// 生成上传url
        /// </summary>
        /// <param name="bucketName">存储桶名称</param>
        /// <param name="objectName">文件唯一Id</param>
        /// <param name="expiresSecond">过期时间</param> 
        /// <returns></returns>
        Task<string> PresignedPutObjectAsync(string bucketName, string objectName, int expiresSecond = 60);
    }
}
