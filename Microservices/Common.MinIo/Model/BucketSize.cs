﻿namespace Common.MinIo.Model
{
    public class BucketSize
    {
        /// <summary>
        /// 存储桶名称
        /// </summary>
        public string BucketName { get; set; }
        /// <summary>
        /// 存储桶大小
        /// </summary>
        public ulong Size { get; set; }
    }
}
