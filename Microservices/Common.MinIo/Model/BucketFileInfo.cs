﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.MinIo.Model
{
    /// <summary>
    /// 文件信息
    /// </summary>
    public class BucketFileInfo
    {
        /// <summary>
        /// 文件key
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 是否是文件夹
        /// </summary>
        public bool IsDir { get; set; }=true;
        /// <summary>
        /// 是否最后一级
        /// </summary>
        public bool IsLatest { get; set; }=false;
        /// <summary>
        /// 文件大小
        /// </summary>
        public ulong Size { get; set; } = 0;
    }
}
