﻿namespace Common.MinIo.Model
{
    /// <summary>
    /// 删除模型
    /// </summary>
    public class DeleteParam
    {

        /// <summary>
        /// 存储桶名称
        /// </summary>
        public string BucketName { get; set; }
        /// <summary>
        /// 文件类型集合
        /// </summary>
        public List<StorageObjectTypeDefine> ObjectTypes { get; set; } = new List<StorageObjectTypeDefine>();
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
    }
}
