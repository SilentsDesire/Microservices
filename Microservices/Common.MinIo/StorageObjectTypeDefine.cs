﻿namespace Common.MinIo
{
    /// <summary>
    /// 存储对象类型
    /// </summary>
    public enum StorageObjectTypeDefine
    {
        /// <summary>
        /// 文件类型
        /// </summary>
        Doc=0,

        /// <summary>
        /// 图片类型
        /// </summary>
        Pic=1,

        /// <summary>
        /// 音频类型
        /// </summary>
        Audio=2,

        /// <summary>
        /// 视频
        /// </summary>
        Video=3,

        /// <summary>
        /// pdf文档
        /// </summary>
        Pdf=4,
    }
}
