﻿namespace Common.MinIo
{
    /// <summary>
    /// minioz相关配置文件
    /// </summary>
    public class MinIoConfig
    {

        /// <summary>
        /// 服务地址 是一个URL，域名，IPv4或者IPv6地址
        /// </summary>
        public string EndPoint { get; set; }

        /// <summary>
        /// 类似于用户ID，用于唯一标识你的账户。可选，为空代表匿名访问
        /// </summary>
        public string AccessKey { get; set; }

        /// <summary>
        /// 是你账户的密码。可选，为空代表匿名访问
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// 请求超时时间(默认5秒)
        /// </summary>
        public int RequestTimeout { get; set; } = 5;

    }
}
