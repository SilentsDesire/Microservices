﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.MinIo.Extensions
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 添加Minio服务
        /// </summary>
        /// <param name="services"></param> 
        /// <param name="section">配置的节点</param> 
        /// <returns></returns>
        public static IServiceCollection AddMinIoConfig(this IServiceCollection services, string section = "MinIo")
        {
            var config = ServiceProviderServiceExtensions.GetRequiredService<IConfiguration>(services.BuildServiceProvider()).GetSection(section).Get<MinIoConfig>();
            //var config = new MinIoConfig();
            //configuration.GetSection("MinIo").Bind(config);
            services.AddSingleton(config);
            services.AddSingleton<IStorageObjectOperate, MinIoObjectOperate>();
            return services;
        }
        /// <summary>
        /// 添加Minio服务
        /// </summary>
        /// <param name="services"></param> 
        /// <param name="config">配置</param> 
        /// <returns></returns>
        public static IServiceCollection AddMinIo(this IServiceCollection services, MinIoConfig config)
        {
            services.AddSingleton(config);
            services.AddSingleton<IStorageObjectOperate, MinIoObjectOperate>();
            return services;
        }
    }
}
