using Autofac;
using Common.Cache;
using Common.CustomLoadBalancer;
using Common.MefHelp;
using Common.Tools;
using Ocelot.Configuration;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;
using Ocelot.ServiceDiscovery.Providers;
using SkyApm.Utilities.DependencyInjection;
using System.Runtime.InteropServices;
using Common.Jwt.Extensions;
namespace Decen.Gateway
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            // 加载配置文件
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var isUseSkywalking = configuration.GetValue<bool>("IsUseSkywalking");
            if (isUseSkywalking) AddSkyWalkingEnvironment();//添加skywalking环境变量，必须放在builder前面

            var builder = WebApplication.CreateBuilder(args);
            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            
            #region 自定义
            //判断当前系统是否为windows
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                builder.Host.UseWindowsService();//加上这句，可以做成windows服务
            }
            var port = configuration.GetValue<int>("Url:Port");
            builder.WebHost.UseUrls(new[] { $"http://*:{(port == 0 ? 8888 : port)}" });
            LogHelper.Info($"初始化服务...端口为:{port}");

            // Add services to the container. 
            if (isUseSkywalking) builder.Services.AddSkyApmExtensions(); //添加Skywalking相关配置  
            builder.Services.AddJwtService();//注入jwt服务
            builder.Services.AddOcelotConfig();//添加ocelot相关配置 
            builder.Services.AddCustomCache(); //注入缓存 
            var container = new ContainerBuilder().AddMefContainer(builder.Services).Completed(); //使用autofac 
            #endregion


            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            //配置跨域
            app.UseCors(options =>
            {
                options.AllowAnyHeader();
                options.AllowAnyMethod();
                options.AllowAnyOrigin();
            });
            app.UseAuthentication();//要在授权之前认证，这个和[Authorize]特性有关
            app.UseAuthorization();
            app.UseOcelot();//使用ocelot

            app.MapControllers();

            app.Run();
        }
        /// <summary>
        /// 添加skywalking环境变量，必须放在builder前面
        /// </summary>
        public static void AddSkyWalkingEnvironment()
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("skyapm.json").Build();
            var serviceName = configuration.GetValue<string>("SkyWalking:ServiceName");
            Environment.SetEnvironmentVariable("ASPNETCORE_HOSTINGSTARTUPASSEMBLIES", "SkyAPM.Agent.AspNetCore");
            Environment.SetEnvironmentVariable("SKYWALKING__SERVICENAME", serviceName);
            LogHelper.Info($"加载skywalking环境变量");
        }
        /// <summary>
        /// 添加Ocelot配置
        /// </summary>
        /// <param name="services"></param>
        public static void AddOcelotConfig(this IServiceCollection services)
        {
            // 从系统中获取 ConfigurationManager 实例
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory()) // 设置基础路径
                .AddJsonFile("ocelot.json", optional: false, reloadOnChange: true) // 加载 ocelot.json 配置
                .Build();

            // 注入 Ocelot 和自定义负载均衡
            Func<IServiceProvider, DownstreamRoute, IServiceDiscoveryProvider, BestResourcesLoadBalancer> loadBalancerFactoryFunc =
                (serviceProvider, route, serviceDiscoveryProvider) => new BestResourcesLoadBalancer(serviceDiscoveryProvider.GetAsync);

            services.AddOcelot(configuration) // 使用加载的配置
                    .AddCustomLoadBalancer(loadBalancerFactoryFunc)
                    .AddConsul<ConsulServiceBuilder>();
        }
    }
}
