using Common.Jwt;
using Common.MefHelp;
using Common.Self;
using Common.Tools.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Decen.ServiceA.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private readonly ILogger<TestController> _logger;

        public TestController(ILogger<TestController> logger)
        {
            _logger = logger;
        }


        [HttpGet]
        public ResponseResult<string> GetIp()
        {
            return ResponseResult<string>.Success($"{SelfConfig.Instance.Ip}:{SelfConfig.Instance.Port}");
        }
        [HttpGet]
        public ResponseResult<string> GetMsg()
        {
            return ResponseResult<string>.Success($"{SelfConfig.Instance.Ip}:{SelfConfig.Instance.Port} 这是ServiceA的信息");
        }
        [HttpGet]
        public async Task<ResponseResult<int>> HandleStationNum(int num)
        {
            var self = Mef.GetService<SelfPush>();
            var stationNum = self.HandleStationNum(num);
            await self.UpdateCache();
            return ResponseResult<int>.Success(stationNum);
        }
        [HttpGet]
        [AllowAnonymous]
        public string GetJwtToken()
        {
            var token = JwtHelper.GetJwtToken(new JwtUserModel()
            {
                UserName = "张三",
                UserId = "32",
                UserType = "admin"
            });
            return token;
        }
    }
}
