﻿//using System.Threading;
//using System.Threading.Tasks;
//using Microsoft.Extensions.Hosting;
//using Rpc.Abstractions;
//using Rpc.Abstractions.ServiceDiscovery;

//namespace Rpc.Core.HostedService
//{
//    public class ServiceDiscoveryStartup : IHostedService
//    {
//        private IServiceDiscovery ServiceDiscovery { get; }

//        private ServerSettings ServerSettings { get; }

//        public ServiceDiscoveryStartup(IServiceDiscovery serviceDiscovery, RpcSettings rpcSettings)
//        {
//            ServiceDiscovery = serviceDiscovery;
//            ServerSettings = rpcSettings.ServerSettings;
//        }

//        public async Task StartAsync(CancellationToken cancellationToken)
//        {
//            if (ServerSettings == null)
//                return;
//            await ServiceDiscovery.RegisterAsync(cancellationToken);
//        }

//        public async Task StopAsync(CancellationToken cancellationToken)
//        {
//            if (ServerSettings == null)
//                return;
//            await ServiceDiscovery.DeregisterAsync();
//        }
//    }
//}
