﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions;

namespace Rpc.Core.HostedService
{
    public class InfrastructureStartup : IHostedService
    {
        private RpcSettings RpcSettings { get; }
        private ILoggerFactory LoggerFactory { get; }


        public InfrastructureStartup(RpcSettings rpcSettings, ILoggerFactory loggerFactory)
        {
            RpcSettings = rpcSettings;
            LoggerFactory = loggerFactory;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            ThreadPool.SetMinThreads(RpcOptions.ThreadPool_MinThreads.Value, RpcOptions.ThreadPool_CompletionPortThreads.Value);
            foreach (var provider in RpcSettings.LoggerProviders)
            {
                LoggerFactory.AddProvider(provider);
            }

            await Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Task.CompletedTask;
        }
    }
}
