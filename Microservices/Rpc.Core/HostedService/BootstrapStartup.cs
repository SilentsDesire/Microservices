﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Rpc.Abstractions;
using Rpc.Remoting;

namespace Rpc.Core.HostedService
{
    public class BootstrapStartup : IHostedService
    {
        private RpcSettings RpcSettings { get; }
        private IBootstrap Bootstrap { get; }

        public BootstrapStartup(RpcSettings rpcSettings, IBootstrap bootstrap)
        {
            RpcSettings = rpcSettings;
            Bootstrap = bootstrap;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (RpcSettings.ServerSettings == null) return;
            await Bootstrap.StartAsync();
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Bootstrap.StopAsync();
        }
    }
}
