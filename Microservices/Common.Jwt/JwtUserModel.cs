﻿namespace Common.Jwt
{
    /// <summary>
    /// jwt用户模型
    /// </summary>
    public class JwtUserModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public string UserType { get; set; }
    }
}
