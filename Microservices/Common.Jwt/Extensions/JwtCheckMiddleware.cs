﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Common.Jwt.Extensions
{
    public class JwtCheckMiddleware
    {
        private readonly RequestDelegate _next;
        public IConfiguration _configuration;
        public JwtCheckMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            _configuration = configuration;
        }

        public async Task Invoke(HttpContext context)
        {
            var myToken = context.Request.Headers["Authorization"].FirstOrDefault();
            if (string.IsNullOrWhiteSpace(myToken))
            {
                context.Response.StatusCode = 401; //401未授权
                await context.Response.WriteAsync("未授权");
                return;
            }
            await _next.Invoke(context);
        }
    }
}
