﻿using Common.MinIo;
using Common.Tools;
using System.Diagnostics;
using System.Reactive.Concurrency;

namespace Test
{
    public static class Program
    {
        private static int index = 0;
        private const string endPoint1 = "192.168.102.133:9000";
        private const string endPoint2 = "192.168.102.90:9000";
        private const string accessKey = "admin";
        private const string secretKey = "12345678";
        //private static List<MinIoObjectOperate> list = new List<MinIoObjectOperate>() {
        //        new MinIoObjectOperate(new MinIoConfig()
        //        {
        //            EndPoint = endPoint1,
        //            AccessKey = accessKey,
        //            SecretKey = secretKey
        //        }),
        //        new MinIoObjectOperate(new MinIoConfig()
        //        {
        //            EndPoint = endPoint2,
        //            AccessKey = accessKey,
        //            SecretKey = secretKey
        //        })
        //};
        private static MinIoObjectOperate _minioClient = new MinIoObjectOperate(new MinIoConfig()
        {
            EndPoint = endPoint2,
            AccessKey = accessKey,
            SecretKey = secretKey
        });
        public static async Task Main(string[] args)
        {

            try
            {
                //await GetObjectAsync();
                await PutObjectAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"启动异常{ex.Message}");
                LogHelper.Error($"启动异常{ex.Message}");
            }

            Console.ReadKey();
        }
        public static async Task PutObjectAsync()
        {

            var filePath = Path.Combine(@"C:\Users\ly\Desktop\频率资源监测效能.docx");
            if (File.Exists(filePath))
            {

                for (var i = 1; i <= 50; i++)
                {
                    Stopwatch stopwatch = Stopwatch.StartNew();
                    stopwatch.Start();
                    using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        await _minioClient.PutObjectAsync("sichuan", $"{DateTime.Now.ToString("yyyyMMddmmssfff")}{i}.docx", file);
                        stopwatch.Stop();
                        LogHelper.Info($"执行时间{i}:{stopwatch.ElapsedMilliseconds}");
                    }
                }
            }
        }
        public static async Task GetObjectAsync()
        {
            for (var i = 1; i <= 50; i++)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                stopwatch.Start();
                var result = await _minioClient.GetObjectAsync("sichuan", $"doc/2024/07/14/2024080704449278.exe");
                if (result != null)
                    result.Dispose();
                stopwatch.Stop();
                LogHelper.Info($"读取时间{i}:{stopwatch.ElapsedMilliseconds}");
            }
        }
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //private static MinIoObjectOperate GetMinioClient()
        //{
        //    var result = list[index];
        //    index = index == 0 ? 1 : 0;
        //    return result;
        //}
    }
}
