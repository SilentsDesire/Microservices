﻿using Common.ServiceDiscovery.Abstractions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.ServiceDiscovery.Implementation
{
    public class ServiceDiscoveryBuilder : IServiceDiscoveryBuilder
    {
        public ServiceDiscoveryBuilder(IServiceCollection services, IConfiguration configuration)
        {
            Services = services;
            Configuration = configuration;
        }

        public IServiceCollection Services { get; private set; }
        public IConfiguration Configuration { get; private set; }
    }
}
