﻿namespace Common.ServiceDiscovery.Model
{
    /// <summary>
    /// 地址
    /// </summary>
    public class HostAndPort
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="downstreamHost"></param>
        /// <param name="downstreamPort"></param>
        public HostAndPort(string downstreamHost, int downstreamPort)
        {
            Address = !string.IsNullOrWhiteSpace(downstreamHost) ? downstreamHost.Trim('/') : string.Empty;
            Port = downstreamPort;
        }

        public string Address { get; private set; }
        public int Port { get; private set; }
        public Uri ToUri(string scheme = "http", string path = "/")
        {
            var builder = new UriBuilder(scheme, this.Address, this.Port, path);
            return builder.Uri;
        }
        public override string ToString()
        {
            return $"{this.Address}:{this.Port}";
        }
    }
}
