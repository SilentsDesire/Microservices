﻿namespace Common.ServiceDiscovery.Model
{
    public class ServiceDiscoveryOption
    {
        /// <summary>
        /// 服务名称
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 健康检查路径
        /// </summary>
        public string HealthCheckUrl { get; set; } 

        /// <summary>
        /// 服务注册类型
        /// </summary>
        public ServiceType ServiceType { get; set; } = ServiceType.HTTP;

        /// <summary>
        /// 自身服务的地址
        /// </summary>
        public string SelfHost { get; set; }
        /// <summary>
        /// 自身服务的端口
        /// </summary>
        public int SelfPort { get; set; }
    }
    public enum ServiceType
    {
        HTTP,
        TCP
    }
}
