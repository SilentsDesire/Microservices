﻿namespace Common.ServiceDiscovery.Abstractions
{
    public interface IServiceDiscovery : IManageServiceInstances,
        IManageHealthChecks,
        IResolveServiceInstances,
        IHaveKeyValues
    {
    }
}
