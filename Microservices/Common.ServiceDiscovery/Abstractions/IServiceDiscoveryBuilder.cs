﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.ServiceDiscovery.Abstractions
{
    public interface IServiceDiscoveryBuilder
    {
        IServiceCollection Services { get; }
        IConfiguration Configuration { get; }
    }
}
