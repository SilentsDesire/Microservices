﻿namespace Common.Self.Computer
{
    /// <summary>
    /// 资源接口
    /// </summary>
    public interface IResource
    { 
        /// <summary>
        /// 获取Cpu
        /// </summary>
        /// <returns></returns>
        Task<float> GetCpuAsync();

        /// <summary>
        /// 获取可用内存
        /// </summary>
        /// <returns></returns>
        Task<float> GetAvailableMemeoryAsync();
    }
}
