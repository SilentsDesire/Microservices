﻿using System.Threading.Tasks;

namespace Rpc.Remoting
{
	public interface IBootstrap
	{

		Task StartAsync();

		Task StopAsync();
	}
}
