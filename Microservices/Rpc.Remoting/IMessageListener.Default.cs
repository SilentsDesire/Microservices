﻿using System.Threading.Tasks;
using Rpc.Abstractions;

namespace Rpc.Remoting
{
    public class MessageListener : IMessageListener
    {
        public event ReceiveMessageHandler OnReceived;

        public async Task Received(TransportMessage<IServiceResult> message)
        {
            OnReceived?.Invoke(message);
            await Task.CompletedTask;
        }
    }
}
