﻿using System;
using System.Threading.Tasks;
using Rpc.Abstractions;

namespace Rpc.Remoting
{


    public interface IClient
    {
        Task<IServiceResult> SendAsync(IInvokeMessage message);

        Task DisconnectAsync();
    }
}
