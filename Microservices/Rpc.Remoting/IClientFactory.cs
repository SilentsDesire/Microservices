﻿using System;
using System.Threading.Tasks; 

namespace Rpc.Remoting
{
    public interface IClientFactory
    {
        Task<IClient> CreateClientAsync(string serviceName, string address,int port);

        Task RemoveClient(string host, int port);

        Task RemoveAllClient();
    }
}
