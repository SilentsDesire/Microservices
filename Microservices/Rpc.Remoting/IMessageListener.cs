﻿using System.Threading.Tasks;
using Rpc.Abstractions;

namespace Rpc.Remoting
{
    public delegate void ReceiveMessageHandler(TransportMessage<IServiceResult> message);
    public interface IMessageListener
    {
        event ReceiveMessageHandler OnReceived;

        Task Received(TransportMessage<IServiceResult> message);
    }
}
