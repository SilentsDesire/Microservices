﻿namespace Rpc.Remoting
{
    public class TransportContext
    {
        public string Host { get; set; }

        public int Port { get; set; }
    }
}
