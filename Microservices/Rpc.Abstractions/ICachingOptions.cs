﻿using System;

namespace Rpc.Abstractions
{
    public interface ICachingOptions
    {
        string KeyPrefix { get; set; }
        int ExpireSeconds { get; set; }
    }
}
