﻿using System;

namespace Rpc.Abstractions.Exceptions
{
	public class NotFoundRouteException : Exception
	{
		public NotFoundRouteException(string route) : base($"Route {route} not found.")
		{
		}
	}
}
