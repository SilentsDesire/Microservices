﻿namespace Rpc.Abstractions.CircuitBreaker
{
    public class ServiceCircuitBreakerOptions : CircuitBreakerOptions
    {
        public bool HasInjection { get; set; }
    }
}
