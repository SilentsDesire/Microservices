﻿namespace Rpc.Abstractions.ServiceDiscovery
{
	public interface IServiceRegisterConfiguration
	{
		string Id { get; set; }
		string Name { get; set; }
	}
}
