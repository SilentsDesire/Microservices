﻿using Common.Tools.Model;
using System;

namespace Rpc.Abstractions
{
    [AttributeUsage(AttributeTargets.Interface)]
    public class ServiceDiscoveryNameAttribute : Attribute
    {

        /// <summary>
        /// 构造函数
        /// </summary>
        public ServiceDiscoveryNameAttribute()
        {
            Name = SelfConfig.Instance.ServiceName;

        }
        public ServiceDiscoveryNameAttribute(string serviceName)
        {
            if (string.IsNullOrWhiteSpace(serviceName))
                throw new ArgumentNullException(nameof(serviceName));
            Name = serviceName;
        }

        public string Name { get; set; }
    }
}
