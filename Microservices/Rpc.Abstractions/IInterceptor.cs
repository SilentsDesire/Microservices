﻿using System.Threading.Tasks;

namespace Rpc.Abstractions
{
    public interface IInterceptor
    {
        Task<IServiceResult> Intercept(IInterceptorContext context);
    }
}
