﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Rpc.Abstractions.ServiceDiscovery;

namespace Rpc.Abstractions
{
    public interface ILoadBalancing
    {
        Task<ServiceNodeInfo> GetNextNode(string serviceName, string serviceRoute, IReadOnlyList<object> serviceArgs, IReadOnlyDictionary<string, string> serviceMeta);
    }
}
