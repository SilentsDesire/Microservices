﻿using System;

namespace Rpc.Abstractions
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Method)]
    public class NonInterceptAttribute : Attribute
    {
    }
}
