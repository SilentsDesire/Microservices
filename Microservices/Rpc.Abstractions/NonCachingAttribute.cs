﻿using System;

namespace Rpc.Abstractions
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Method)]
    public class NonCachingAttribute : Attribute
    {
    }
}
