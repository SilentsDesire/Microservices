﻿using System.Threading.Tasks;

namespace Rpc.Abstractions.Service
{
    public interface IMethodInvoker
    {
        Task<object> InvokeAsync(object instance, params object[] args);
    }
}
