﻿namespace Rpc.Abstractions
{
    public class EnvironmentVariables
    {
        public const string rpc_server_addr = "rpc_server_addr";

        public const string rpc_server_port = "rpc_server_port";

        public const string rpc_server_weight = "rpc_server_weight";

        public const string rpc_service_id = "rpc_service_id";

        public const string rpc_service_name = "rpc_service_name";

        public const string rpc_consul_addr = "rpc_consul_addr";

        public const string rpc_consul_token = "rpc_consul_token";

        public const string rpc_consul_dc = "rpc_consul_dc";

        public const string rpc_consul_timeout = "rpc_consul_timeout";

        public const string rpc_consul_hc_interval = "rpc_consul_hc_interval";

        public const string rpc_zk_addr = "rpc_zk_addr";

        public const string rpc_zk_session_timeout = "rpc_zk_session_timeout";

        public const string rpc_zk_readonly = "rpc_zk_readonly";
    }
}
