﻿using DotNetCore.CAP;

namespace Common.CAP
{
    public class TestHandler : ICapSubscribe
    {
        [CapSubscribe("order.created")]
        public void HandleOrderCreated(OrderCreatedEvent orderEvent)
        {
            // 处理事件的代码，例如发送通知或更新数据库
            Console.WriteLine($"Order created: {orderEvent.OrderId}, {orderEvent.OrderDate}, {orderEvent.CustomerName}");
        }
    }
    public class OrderService
    {
        private readonly ICapPublisher _capBus;

        public OrderService(ICapPublisher capBus)
        {
            _capBus = capBus;
        }

        public async Task CreateOrderAsync(OrderCreatedEvent orderEvent)
        {
            // 业务逻辑代码，例如创建订单

            // 发布事件
            await _capBus.PublishAsync("order.created", orderEvent);
        }
    }
    public class OrderCreatedEvent
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string? CustomerName { get; set; }
    }

}
