using Common.MinIo;
using Common.Tools;
using System.Diagnostics;

namespace MinIoTest
{
    [TestClass]
    public class MinIoTest
    {
        private const string endPoint = "192.168.102.133:9000";
        private const string accessKey = "admin";
        private const string secretKey = "12345678";
        private MinIoObjectOperate _minioClient = new MinIoObjectOperate(new MinIoConfig()
        {
            EndPoint = endPoint,
            AccessKey = accessKey,
            SecretKey = secretKey
        });
        [TestMethod]
        public async Task PutObjectAsync()
        {

            var filePath = Path.Combine(@"D:\software\wechat\WeChatSetup.exe");
            if (File.Exists(filePath))
            {

                for (var i = 1; i <= 100; i++)
                {
                    Stopwatch stopwatch = Stopwatch.StartNew();
                    stopwatch.Start();
                    using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        await _minioClient.PutObjectAsync("Report", $"File/{DateTime.Now.ToString("yyyyMMddmmssfff")}{i}.exe", file);
                        stopwatch.Stop();
                        LogHelper.Info($"ִ��ʱ��{i}:{stopwatch.ElapsedMilliseconds}");
                    }
                }
            }
        }
        [TestMethod]
        public async Task GetObjectAsync()
        {
            var stream1 = await _minioClient.GetObjectAsync("test", $"docker-compose.yml");

        }
        [TestMethod]
        public async Task GetListBucketAsync()
        {
            await _minioClient.SearchObjectsAsync("chengdu/doc/2024/7/13");

        }
        [TestMethod]
        public async Task PresignedGetObjectAsync()
        {
            var url = await _minioClient.PresignedGetObjectAsync("sichuan", $"doc/2024/07/14/docker-compose.yml");

        }
        [TestMethod]
        public async Task GetBucketSizeAsync()
        {
            var size = await _minioClient.GetBucketSizeAsync();
            Assert.IsNotNull(size);
        }
        [TestMethod]
        public async Task DeleteObjectsByConditionAsync()
        {
            var result = await _minioClient.DeleteObjectsByConditionAsync(new Common.MinIo.Model.DeleteParam()
            {
                BucketName = "sichuan",
                ObjectTypes = new List<StorageObjectTypeDefine> { StorageObjectTypeDefine.Doc },
                StartTime = DateTime.Now.AddDays(-30),
                EndTime = DateTime.Now,
            });
            Assert.IsTrue(result);
        }
    }
}