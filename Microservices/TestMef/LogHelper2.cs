﻿ 
using System.ComponentModel.Composition;

namespace TestMef
{
    public class LogHelper2
    {
        private readonly ILogger<LogHelper2> _logger;
        public LogHelper2(ILogger<LogHelper2> logger)
        {
            _logger = logger;
        } 

        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        public void Debug(String msg)
        {
            _logger.LogDebug(msg);
        }

        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public void Info(String msg)
        {
            _logger.LogInformation(msg);
        }

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public void Error(String msg)
        {
            _logger.LogError(msg);
        }


        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public void Warn(String msg)
        {
            _logger.LogWarning(msg);
        }
    }
}
