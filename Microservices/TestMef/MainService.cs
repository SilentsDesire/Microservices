﻿using PeterKottas.DotNetCore.WindowsService.Base;
using PeterKottas.DotNetCore.WindowsService.Interfaces;

namespace TestMef
{
    public class MainService : IMicroService
    {
        private IMicroServiceController controller;

        public MainService(IMicroServiceController controller)
        {
            this.controller = controller;
        }

        public void Start()
        {
            Console.WriteLine("I started"); 
        }

        public void Stop()
        {
            Console.WriteLine("I stopped");
        }
    }
}
