using Autofac;
using NLog.Web;
using PeterKottas.DotNetCore.WindowsService;

namespace TestMef
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Logging.AddNLog("NLog.config");
            builder.Services.AddSingleton<LogHelper2>();
            var container = new ContainerBuilder().AddMefContainer().Compelted();
            //builder.Services.AddMefContainer();

            var result = Mef.GetService<TestMefExport>().Get();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthorization();


            app.MapControllers(); 
            //ServiceRunner<MainService>.Run(config =>
            //{
            //    var name = config.GetDefaultName();
            //    config.Service(serviceConfig =>
            //    {
            //        serviceConfig.ServiceFactory((extraArguments, controller) =>
            //        {
            //            return new MainService(controller);
            //        });

            //        serviceConfig.OnStart((service, extraParams) =>
            //        {
            //            Console.WriteLine("Service {0} started", name);
            //            service.Start();
            //        });

            //        serviceConfig.OnStop(service =>
            //        {
            //            Console.WriteLine("Service {0} stopped", name);
            //            service.Stop();
            //        });

            //        serviceConfig.OnError(e =>
            //        {
            //            Console.WriteLine("Service {0} errored with exception : {1}", name, e.Message);
            //        });
            //    });
            //});
            app.Run();
        }
    }
}
