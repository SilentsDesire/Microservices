﻿using Microsoft.AspNetCore.Mvc;
using Rpc.Service.TestInterfaces; 

namespace TestMef
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ValuesController : ControllerBase
    {
        //private IHelloService HelloService { get; }


        //private IPersonService PersonService { get; }

        //public ValuesController(IHelloService helloService, IPersonService personService)
        //{
        //    HelloService = helloService;
        //    PersonService = personService;
        //}

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            //var a1 = await PersonService.GetName(1);
            //var a = Guid.NewGuid().ToString();
            ////var r = ServiceProvider.GetServices<IRemotingInvoke>();
            //var r = await HelloService.SetMeta(("token", "bearer .....")).SayHello(a);
            //if (r.Message == a)
            //    return Ok(r);
            var res = await Mef.GetService<IPersonService>().GetName(22);
            return Ok(res);
        }

    }
}
