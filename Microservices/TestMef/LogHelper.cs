﻿using NLog;
using NLog.Config;

namespace TestMef
{
    public static class LogHelper
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger(); //初始化日志类
        /// <summary>
        /// 静态构造函数
        /// </summary>
        static LogHelper()
        {
            //初始化配置日志
            LogManager.Configuration = new XmlLoggingConfiguration(AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\NLog.config");
        }


        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        public static void Debug(String msg)
        {
            _logger.Debug(msg);
        }

        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Info(String msg)
        {
            _logger.Info(msg);
        }

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Error(String msg)
        {
            _logger.Error(msg);
        }

        /// <summary>
        /// 严重致命错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Fatal(String msg)
        {
            _logger.Fatal(msg);
        }

        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Warn(String msg)
        {
            _logger.Warn(msg);
        }
    }
}
