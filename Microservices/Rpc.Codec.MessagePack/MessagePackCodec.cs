﻿using System;
using MessagePack;
using MessagePack.Resolvers;
using Rpc.Abstractions;

namespace Rpc.Codec.MessagePack
{
    public class MessagePackCodec : ICodec
    {
        public MessagePackCodec()
        {
            CompositeResolver.Create(NativeDateTimeResolver.Instance, ContractlessStandardResolverAllowPrivate.Instance);
            MessagePackSerializer.DefaultOptions.WithResolver(ContractlessStandardResolverAllowPrivate.Instance);
        }

        public byte[] Serialize<TData>(TData data)
        {
            return MessagePackSerializer.Typeless.Serialize(data);
        }

        public object Deserialize(byte[] data, Type type)
        {
            return data == null || data.Length == 0 ? null : MessagePackSerializer.Typeless.Deserialize(data);
        }

        public T Deserialize<T>(byte[] data)
        {
            return data == null || data.Length == 0 ? default : (T)MessagePackSerializer.Typeless.Deserialize(data);
        }

        public string ToJson<TData>(TData data)
        {
            return data == null ? default : MessagePackSerializer.SerializeToJson(data);
        }
    }
}
