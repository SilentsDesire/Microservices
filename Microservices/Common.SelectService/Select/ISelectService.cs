﻿using Common.ServiceDiscovery.Model;

namespace Common.SelectService.Select
{
    /// <summary>
    /// 查询服务接口
    /// </summary>
    public interface ISelectService
    {
        /// <summary>
        /// 获取Ip和端口
        /// </summary>
        /// <param name="serviceName">服务名称</param>
        /// <returns></returns>
        Task<string?> GetHostAndPortAsync(string serviceName);
    }
}
