﻿using Common.MefHelp.Attributes;
using Common.ServiceDiscovery.Model;
using Common.Tools;
namespace Common.SelectService.Select
{
    /// <summary>
    /// 网关帮助查询服务
    /// </summary>
    [CustomInjection("gateway", typeof(ISelectService))]
    public class GatewaySelectService : ISelectService
    {


        /// <summary>
        /// 获取Ip和端口
        /// </summary>
        /// <param name="serviceName">服务名称</param>
        /// <returns></returns>
        public async Task<string?> GetHostAndPortAsync(string serviceName)
        {
            if (string.IsNullOrWhiteSpace(serviceName))
            {
                LogHelper.Error($"服务名称不能为空");
                return null;
            }
            if (SelectServiceConfig.Instance == null || string.IsNullOrWhiteSpace(SelectServiceConfig.Instance.GatewayUrl))
            {
                LogHelper.Error($"获取网关地址为空");
                return null;
            }
            var url = $"{(SelectServiceConfig.Instance.GatewayUrl.Contains("http") ? SelectServiceConfig.Instance.GatewayUrl : ("http://" + SelectServiceConfig.Instance.GatewayUrl)).TrimEnd('/')}/{serviceName}";
            return url;

        }
    }
}
