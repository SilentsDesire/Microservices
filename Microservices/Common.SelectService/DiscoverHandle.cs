﻿using Common.MefHelp;
using Common.MefHelp.Attributes;
using Common.SelectService.Select;
using Common.Tools;

namespace Common.SelectService
{
    /// <summary>
    /// 发现处理
    /// </summary> 
    [CustomInjection]
    public class DiscoverHandle
    {

        /// <summary>
        ///  选择服务
        /// </summary>
        /// <param name="serviceName">服务名称</param>
        /// <returns></returns>
        public async Task<string?> SelectService(string serviceName)
        {
            if (SelectServiceConfig.Instance == null || string.IsNullOrWhiteSpace(SelectServiceConfig.Instance.SelectServiceType))
            {
                LogHelper.Error($"获取服务选择类型为空");
                return null;
            }
            var url = await Mef.GetService<ISelectService>(SelectServiceConfig.Instance.SelectServiceType.ToLower()).GetHostAndPortAsync(serviceName);
            return url;
        }
    }
}
