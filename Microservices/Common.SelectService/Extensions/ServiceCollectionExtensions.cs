﻿using Common.LoadBalancer.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.SelectService.Extensions
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 添加选择服务
        /// </summary>
        /// <param name="services"></param> 
        /// <returns></returns>
        public static IServiceCollection AddSelectService(this IServiceCollection services, string section = "SelectServiceOption")
        {
            services.AddLoadBalancer();
            //services.AddSingleton<DiscoverHandle>();
            var selectServiceOption = ServiceProviderServiceExtensions.GetRequiredService<IConfiguration>(services.BuildServiceProvider()).GetSection(section).Get<SelectServiceOption>();
            if (selectServiceOption != null)
            {
                SelectServiceConfig.SetInstance(selectServiceOption);
            }
            return services;
        }
    }
}
