﻿namespace Common.SelectService
{
    /// <summary>
    /// 查询服务配置文件
    /// </summary>
    public class SelectServiceOption
    {
        /// <summary>
        /// 查询服务类型(Gateway、Consul),默认网关
        /// </summary>
        public string SelectServiceType { get; set; } = "Gateway";

        /// <summary>
        /// 网关地址( SelectServiceType=Gateway使用)
        /// </summary>
        public string GatewayUrl { get; set; }

        /// <summary>
        /// 负载均衡类型( SelectServiceType=Consul使用),默认轮询
        /// </summary>
        public string LoadBalancerType { get; set; } = "RoundRobin";
    }
    public class SelectServiceConfig : SelectServiceOption
    {
        #region 字段

        /// <summary>
        /// 实例
        /// </summary>
        private static SelectServiceConfig _instance;
        /// <summary>
        /// 静态实例
        /// </summary>
        public static SelectServiceConfig Instance
        {
            get
            {
                return _instance;
            }
        }
        #endregion
        /// <summary>
        /// 静态实例
        /// </summary>
        public static void SetInstance(SelectServiceOption option)
        {
            if (_instance == null)
            {
                _instance = new SelectServiceConfig
                {
                    GatewayUrl = option.GatewayUrl,
                    LoadBalancerType = option.LoadBalancerType,
                    SelectServiceType = option.SelectServiceType,
                };
            }
        }
    }
}
