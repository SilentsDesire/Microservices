using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using System.Runtime.InteropServices;

namespace TestGateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            // 加载配置文件
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            builder.Configuration.AddJsonFile("ocelot.json", optional: false, reloadOnChange: true);
            //判断当前系统是否为windows
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                builder.Host.UseWindowsService();//加上这句，可以做成windows服务
            }
            var port = configuration.GetValue<int>("Url:Port");
            builder.WebHost.UseUrls(new[] { $"http://*:{(port == 0 ? 8888 : port)}" });
            // Add services to the container. 
            builder.Services.AddCors(options =>
            { 
                options.AddPolicy("Cors", builder =>
                {
                    builder.AllowAnyMethod()
                                 .SetIsOriginAllowed(_ => true)
                                 .AllowAnyHeader()
                                 .AllowCredentials();
                });
            });
            builder.Services.AddOcelot();

            var app = builder.Build();

            // Configure the HTTP request pipeline. 
            app.UseCors("Cors");
            //使用ocelot
            app.UseOcelot();

            app.Run();
        }
    }
}
