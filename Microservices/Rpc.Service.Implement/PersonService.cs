﻿using System;
using System.Threading.Tasks;
using Rpc.Service.TestInterfaces;

namespace Rpc.Service.Implement
{

    public class PersonService : IPersonService
    {
        private IHelloService HelloService { get; }

        public PersonService(IHelloService helloService)
        {
            HelloService = helloService;
        }

        public async Task<object> GetName(int id)
        {
            //await Task.Delay(TimeSpan.FromHours(1));
            return await Task.FromResult(new
            {
                name = $"[{id}]Owen",
                //message = await HelloService.SayHello("Owen")
            });
        }
    }
}
