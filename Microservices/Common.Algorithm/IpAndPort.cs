﻿namespace Common.Algorithm
{
    /// <summary>
    /// Ip和端口
    /// </summary>
    public class IpAndPort
    {
        /// <summary>
        /// 地址IP
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        public IpAndPort() { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public IpAndPort(string ip, int port)
        {
            Ip = ip;
            Port = port;
        }
        public override string ToString()
        {
            return $"{Ip.Replace(".", "_")}_{Port}";
        }
    }
}
