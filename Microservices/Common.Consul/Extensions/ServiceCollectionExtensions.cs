﻿using Common.ServiceDiscovery.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Common.Consul.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceDiscoveryBuilder UseConsul(this IServiceDiscoveryBuilder builder)
        {
            builder.Services.Configure<ConsulServiceDiscoveryConfiguration>(builder.Configuration.GetSection("ServiceDiscovery:Consul"));
            builder.Services.AddSingleton<IServiceDiscovery, ConsulServiceDiscovery>();
            return builder;
        }
    }
}
