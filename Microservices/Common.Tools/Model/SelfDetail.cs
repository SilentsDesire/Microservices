﻿namespace Common.Tools.Model
{
    /// <summary>
    /// 子服务自身信息
    /// </summary>
    public class SelfDetail
    {
        /// <summary>
        /// Ip地址
        /// </summary>
        public string Ip { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 站的数量
        /// </summary>
        public long StationNum { get; set; }

        /// <summary>
        /// Cpu信息
        /// </summary>
        public float Cpu { get; set; }

        /// <summary>
        /// 可用内存
        /// </summary>
        public float AvailableMemeory { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; } = DateTime.Now;
    }
}
