﻿using System.Text.Json.Serialization;

namespace Common.Tools.Model
{
    /// <summary>
    /// 响应结果
    /// </summary>
    [Serializable]
    public class ResponseResult<T>: ResposeModel
    { 
        /// <summary>
        /// 响应数据
        /// </summary>
        [JsonPropertyName("data")]
        public T? Data { get; set; }

        /// <summary>
        /// 内部错误
        /// </summary>
        /// <returns></returns>
        public static ResponseResult<T> Error()
        {
            return new ResponseResult<T>()
            {
                Code = 500,
                Message = "内部错误"
            };
        }
        /// <summary>
        /// 成功
        /// </summary>
        /// <returns></returns>
        public static ResponseResult<T> Success()
        {
            return new ResponseResult<T>()
            {
                Code = 200,
                Message = "成功"
            };
        }
        /// <summary>
        /// 成功
        /// </summary>
        /// <returns></returns>
        public static ResponseResult<T> Success(T data, string? msg = null)
        {
            return new ResponseResult<T>()
            {
                Code = 200,
                Message = string.IsNullOrWhiteSpace(msg) ? "成功" : msg,
                Data = data
            };
        }
    }

    public class ResposeModel
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [JsonPropertyName("code")]
        public int Code { get; set; }
        /// <summary>
        /// 响应消息
        /// </summary>
        [JsonPropertyName("message")]
        public string? Message { get; set; }
    }
}
