﻿using Microsoft.Extensions.Logging;

namespace Common.Tools
{

    public class LoggerUtil
    {
        private static readonly ILogger _logger;
        static LoggerUtil()
        {
            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddConsole();
            });

            _logger = loggerFactory.CreateLogger<LoggerUtil>();
        }
        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        public static void Debug(String msg)
        {
            _logger.LogDebug(msg);
        }

        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Info(String msg)
        {
            _logger.LogInformation(msg);
        }

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Error(String msg)
        {
            _logger.LogError(msg);
        }

        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Warn(String msg)
        {
            _logger.LogWarning(msg);
        }
    }
}
