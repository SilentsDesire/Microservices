﻿using NLog;
using NLog.Config;

namespace Common.Tools
{
    public static class LogHelper
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger(); //初始化日志类
        /// <summary>
        /// 静态构造函数
        /// </summary>
        static LogHelper()
        {
            //初始化配置日志
            LogManager.Configuration = new XmlLoggingConfiguration(AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\NLog.config");
        }


        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        public static void Debug(String msg)
        {
            _logger.Debug(msg);
        }

        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Debug(String msg, Exception ex)
        {
            _logger.Debug(msg, ex);
        }
        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Debug(String msg, params object[] args)
        {
            _logger.Debug(msg, args);
        }
        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Debug(String msg, Exception ex, params object[] args)
        {
            _logger.Debug(msg, ex, args);
        }
        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Debug(Exception ex, String msg)
        {
            _logger.Debug(ex, msg);
        }

        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Debug(Exception ex, String msg, params object[] args)
        {
            _logger.Debug(ex, msg, args);
        }



        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Info(String msg)
        {
            _logger.Info(msg);
        }

        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Info(String msg, Exception ex)
        {
            _logger.Info(msg, ex);
        }
        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Info(String msg, params object[] args)
        {
            _logger.Info(msg, args);
        }
        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Info(String msg, Exception ex, params object[] args)
        {
            _logger.Info(msg, ex, args);
        }
        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Info(Exception ex, String msg)
        {
            _logger.Info(ex, msg);
        }

        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Info(Exception ex, String msg, params object[] args)
        {
            _logger.Info(ex, msg, args);
        }



        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Error(String msg)
        {
            _logger.Error(msg);
        }
        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="ex">日志内容</param> 
        public static void Error(Exception ex)
        {
            _logger.Error(ex);
        }
        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Error(String msg, Exception ex)
        {
            _logger.Error(msg, ex);
        }
        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Error(String msg, params object[] args)
        {
            _logger.Error(msg, args);
        }
        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Error(String msg, Exception ex, params object[] args)
        {
            _logger.Error(msg, ex, args);
        }
        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Error(Exception ex, String msg)
        {
            _logger.Error(ex, msg);
        }

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Error(Exception ex, String msg, params object[] args)
        {
            _logger.Error(ex, msg, args);
        }



        /// <summary>
        /// 严重致命错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Fatal(String msg)
        {
            _logger.Fatal(msg);
        }
        /// <summary>
        /// 严重致命错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Fatal(String msg, Exception ex)
        {
            _logger.Fatal(msg, ex);
        }
        /// <summary>
        /// 严重致命错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Fatal(String msg, params object[] args)
        {
            _logger.Fatal(msg, args);
        }
        /// <summary>
        /// 严重致命错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Fatal(String msg, Exception ex, params object[] args)
        {
            _logger.Fatal(msg, ex, args);
        }
        /// <summary>
        /// 严重致命错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Fatal(Exception ex, String msg)
        {
            _logger.Fatal(ex, msg);
        }

        /// <summary>
        /// 严重致命错误日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Fatal(Exception ex, String msg, params object[] args)
        {
            _logger.Fatal(ex, msg, args);
        }



        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Warn(String msg)
        {
            _logger.Warn(msg);
        }
        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Warn(String msg, Exception ex)
        {
            _logger.Warn(msg, ex);
        }
        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Warn(String msg, params object[] args)
        {
            _logger.Warn(msg, args);
        }
        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Warn(String msg, Exception ex, params object[] args)
        {
            _logger.Warn(msg, ex, args);
        }
        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Warn(Exception ex, String msg)
        {
            _logger.Warn(ex, msg);
        }

        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="msg">日志内容</param> 
        public static void Warn(Exception ex, String msg, params object[] args)
        {
            _logger.Warn(ex, msg, args);
        }


    }
}
