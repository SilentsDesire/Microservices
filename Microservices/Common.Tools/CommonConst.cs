﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Tools
{
    /// <summary>
    /// 公共常量
    /// </summary>
    public class CommonConst
    {
        /// <summary>
        /// 子服务的key
        /// </summary>
        public const string ChildDetailKey = "ChildService";
    }
}
