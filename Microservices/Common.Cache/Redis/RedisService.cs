﻿using Common.Tools;
using StackExchange.Redis;
using System.Text.Json;

namespace Common.Cache.Redis
{
    /// <summary>
    /// redis缓存服务
    /// </summary>
    public class RedisService : ICustomCache
    {
        /// <summary>
        /// redis实例
        /// </summary>
        private readonly IDatabase _redis;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="client"></param>
        public RedisService(RedisHelper client)
        {
            _redis = client.GetDatabase();
        }



        /// <summary>
        /// 设置key
        /// </summary>
        /// <param name="key">名称</param>
        /// <param name="values">值</param>
        /// <param name="timeSpan">过期时间</param>
        /// <returns></returns>
        public async Task<bool> SetAsync<T>(string key, T values, TimeSpan timeSpan)
        {
            try
            {
                return await _redis.StringSetAsync(key, JsonSerializer.Serialize(values), timeSpan);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// 获取key
        /// </summary>
        /// <param name="key">名称</param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(string key)
        {
            try
            {
                var result = await _redis.StringGetAsync(key);
                if (result.HasValue)
                {
                    var model = JsonSerializer.Deserialize<T>(result.ToString());
                    return model;
                }
                return default;

            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return default;
            }
        }

        /// <summary>
        /// 删除key
        /// </summary>
        /// <param name="key">名称</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string key)
        {
            try
            {
                return await _redis.KeyDeleteAsync(key);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 库存自增
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">增长的数量</param>
        /// <returns></returns>
        public async Task<long> IncrementAsync(string key, long value = 1)
        {
            try
            {
                return await _redis.StringIncrementAsync(key, value);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return -404;
            }
        }
        /// <summary>
        /// 获取锁
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="token">令牌</param>
        /// <param name="timeSpan">过期时间</param>
        /// <returns></returns>
        public async Task<bool> LockTakeAsync(string key, string token, TimeSpan timeSpan)
        {
            try
            {
                return await _redis.LockTakeAsync(key, token, timeSpan);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// 释放锁
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="token">令牌</param> 
        /// <returns></returns>
        public async Task<bool> LockReleaseAsync(string key, string token)
        {
            try
            {
                return await _redis.LockReleaseAsync(key, token);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return false;
            }
        }
    }
}
