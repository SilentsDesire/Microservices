﻿using System.ComponentModel.DataAnnotations;

namespace Common.Cache.Redis
{
    /// <summary>
    /// Redis配置
    /// </summary>
    public class RedisConfig
    {
        /// <summary>
        /// 实例名称
        /// </summary>
        [Required]
        public string InstanceName { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }


        /// <summary>
        /// 默认数据库
        /// </summary>
        public int DefaultDB { get; set; }

        /// <summary>
        /// 连接信息
        /// </summary>
        [Required]
        public List<ConnectConfig> Endpoints { get; set; }


    }
    /// <summary>
    /// 连接信息
    /// </summary>
    public class ConnectConfig
    {
        /// <summary>
        /// Ip
        /// </summary>
        [Required]
        public string Host { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        [Required]
        public int Port { get; set; }
    }
}
