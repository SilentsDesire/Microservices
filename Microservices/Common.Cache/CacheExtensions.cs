﻿using Common.Cache.Redis;
using Common.Tools;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.Cache
{
    public static class CacheExtensions
    {
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="services"></param>
        public static void AddCustomCache(this IServiceCollection services)
        {
            try
            {
                var provider = ServiceProviderServiceExtensions.GetRequiredService<IConfiguration>(services.BuildServiceProvider());
                //redis缓存
                var type = provider.GetSection("CacheConfig:Type")?.Value;
                type = string.IsNullOrWhiteSpace(type) ? "Redis" : type;
                switch (type)
                {
                    case "Redis":
                        var redisConfig = provider.GetSection("CacheConfig:Redis").Get<RedisConfig>();
                        services.AddSingleton(new RedisHelper(redisConfig));
                        services.AddSingleton<ICustomCache, RedisService>();
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }

        }
    }
}
