﻿namespace Common.Cache
{
    /// <summary>
    /// 自定义缓存接口
    /// </summary>
    public interface ICustomCache
    {
        /// <summary>
        /// 设置key
        /// </summary>
        /// <param name="key">名称</param>
        /// <param name="values">值</param>
        /// <param name="timeSpan">过期时间</param>
        /// <returns></returns>
        Task<bool> SetAsync<T>(string key, T values, TimeSpan timeSpan);
        /// <summary>
        /// 获取key
        /// </summary>
        /// <param name="key">名称</param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string key);
        /// <summary>
        /// 删除key
        /// </summary>
        /// <param name="key">名称</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(string key);
        /// <summary>
        /// 库存自增
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">增长的数量</param>
        /// <returns></returns>
        Task<long> IncrementAsync(string key, long value = 1);

        /// <summary>
        /// 获取锁
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="token">令牌</param>
        /// <param name="timeSpan">过期时间</param>
        /// <returns></returns>
        Task<bool> LockTakeAsync(string key, string token, TimeSpan timeSpan);

        /// <summary>
        /// 释放锁
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="token">令牌</param> 
        /// <returns></returns>
        Task<bool> LockReleaseAsync(string key, string token);
    }
}
