﻿using Common.ServiceDiscovery.Model;

namespace Common.LoadBalancer.Abstractions
{
    public interface ILoadBalancer
    {
        Task<HostAndPort> Lease();
        void Release(HostAndPort hostAndPort);
    }
}
