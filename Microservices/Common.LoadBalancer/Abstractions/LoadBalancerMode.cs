﻿namespace Common.LoadBalancer.Abstractions
{
    public enum LoadBalancerMode
    {
        Random,
        RoundRobin,
        LeastConnection,
        Hash,
        BestResources,
    }
}