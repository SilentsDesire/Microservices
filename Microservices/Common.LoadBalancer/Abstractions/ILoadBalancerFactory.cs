﻿namespace Common.LoadBalancer.Abstractions
{
    public interface ILoadBalancerFactory
    {
        Task<ILoadBalancer> Get(string serviceName, LoadBalancerMode loadBalancer = LoadBalancerMode.RoundRobin);
    }
}
