﻿using Common.LoadBalancer.Abstractions;
using Common.ServiceDiscovery.Model;

namespace Common.LoadBalancer.Selectors
{
    public class RandomSelector : ILoadBalancer
    {
        private readonly Func<Task<IList<ServiceInformation>>> _services;
        private readonly string _serviceName;
        private readonly Func<int, int, int> _generate;
        private readonly Random _random;
        public RandomSelector(Func<Task<IList<ServiceInformation>>> services, string serviceName)
        {
            _services = services;
            _serviceName = serviceName;
            _random = new Random();
            _generate = (min, max) => _random.Next(min, max);
        }

        public async Task<HostAndPort> Lease()
        {
            var services = await _services.Invoke() ?? new List<ServiceInformation>();

            if (services == null || services.Count == 0)
                throw new Exception($"{_serviceName}没有找到实例");

            var index = _generate(0, services.Count);

            return services[index].HostAndPort;
        }

        public void Release(HostAndPort hostAndPort)
        {
        }
    }
}
