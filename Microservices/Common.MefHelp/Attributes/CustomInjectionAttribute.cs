﻿namespace Common.MefHelp.Attributes
{
    /// <summary>
    /// 指定某个类型、属性、字段或方法提供特定的注入。
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field,
        AllowMultiple = true, Inherited = false)]
    public class CustomInjectionAttribute : Attribute
    {
        /// <summary>
        /// 获取用于注入使用此特性标记的类型或成员的协定名称
        /// </summary>
        public string? ContractName { get; }

        /// <summary>
        /// 获取由此特性附加到的成员注入的协定类型。
        /// </summary>
        public Type? ContractType { get; }

        /// <summary>
        ///  通过在默认协定名称下注入使用此特性标记的类型或成员
        /// </summary>
        public CustomInjectionAttribute()
        {
        }

        /// <summary>
        /// 通过在派生自指定类型的协定名称下注入使用此特性标记的类型或成员
        /// </summary>
        /// <param name="contractType">从中派生用于注入使用此特性标记的类型或成员的协定名称的类型，或 null 以使用默认协定名称。</param>
        public CustomInjectionAttribute(Type? contractType)
        {
            ContractType = contractType;
        }

        /// <summary>
        ///  通过在指定的协定名称下注入用此属性标记的类型或成员
        /// </summary>
        /// <param name="contractName">用于注入使用此特性标记的类型或成员的协定名称，或 null 或空字符串 ("") 以使用默认协定名称。</param>
        public CustomInjectionAttribute(string? contractName)
        {
            ContractName = contractName;
        }

        /// <summary>
        ///   通过在指定协定名称下注入指定类型
        /// </summary>
        /// <param name="contractName">用于注入使用此特性标记的类型或成员的协定名称，或 null 或空字符串 ("") 以使用默认协定名称。</param>
        /// <param name="contractType"> 要注入的类型</param>
        public CustomInjectionAttribute(string? contractName, Type? contractType)
        {
            ContractName = contractName;
            ContractType = contractType;
        }
    }
}