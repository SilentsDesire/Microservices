﻿using Common.MefHelp.Enums;

namespace Common.MefHelp.Attributes
{
    /// <summary>
    ///  指定部件的 System.ComponentModel.Composition.PartCreationPolicyAttribute.CreationPolicy。
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class PartCreationPolicyAttribute : Attribute
    {
        //
        // 摘要:
        //     使用指定的创建策略初始化 System.ComponentModel.Composition.PartCreationPolicyAttribute 类的新实例。
        //
        // 参数:
        //   creationPolicy:
        //     要使用的创建策略。
        public PartCreationPolicyAttribute(CreationPolicy creationPolicy)
        {
            CreationPolicy = creationPolicy;
        }

        //
        // 摘要:
        //     获取或设置一个值，该值指示特性化部件的创建策略。
        //
        // 返回结果:
        //     System.ComponentModel.Composition.PartCreationPolicyAttribute.CreationPolicy
        //     值之一，指示特性化部件的创建策略。 默认值为 System.ComponentModel.Composition.CreationPolicy.Any。
        public CreationPolicy CreationPolicy { get; }
    }
}
