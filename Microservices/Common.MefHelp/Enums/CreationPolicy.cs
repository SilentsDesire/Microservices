﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.MefHelp.Enums
{
    /// <summary>
    ///  指定何时以及如何实例化部件。
    /// </summary>
    public enum CreationPolicy
    {
        //
        // 摘要:
        //    同一个Lifetime生成的对象是同一个实例
        InstancePerLifetimeScope = 0,
        //
        // 摘要:
        //  单例模式，每次调用，都会使用同一个实例化的对象；每次都用同一个对象；
        SingleInstance = 1,
        //
        // 摘要:
        //   默认模式，每次调用，都会重新实例化对象；每次请求都创建一个新的对象；
        InstancePerDependency = 2
    }
}
