﻿using System.Threading.Tasks;
using Common.Tools;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions;
using Rpc.DynamicProxy.Interceptor;

namespace Rpc.DynamicProxy.Common
{
    public class ClientMethodInterceptorAttribute : InterceptorAttributeAbstract
    {
        //private ILogger Logger { get; }

        //public ClientMethodInterceptorAttribute(ILogger<ServerInterceptorAttribute> logger)
        //{
        //    Logger = logger;
        //}

        public ClientMethodInterceptorAttribute()
        {
        }

        public override async Task<IServiceResult> Intercept(IInterceptorContext context)
        {
            LogHelper.Info("\n------------------>Client method interceptor attribute\n");
            var r = await context.Next();
            return r;
        }
    }
}
