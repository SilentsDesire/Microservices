﻿using System.Threading.Tasks;
using Common.Tools;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions;
using Rpc.DynamicProxy.Interceptor;

namespace Rpc.DynamicProxy.Common
{
    public class ServerClassInterceptorAttribute : InterceptorAttributeAbstract
    {
        //private ILogger Logger { get; }

        //public ServerClassInterceptorAttribute(ILogger<ServerInterceptorAttribute> logger)
        //{
        //    Logger = logger;
        //}

        public ServerClassInterceptorAttribute()
        {
        }

        public override async Task<IServiceResult> Intercept(IInterceptorContext context)
        {
            LogHelper.Info("\n------------------>Server class interceptor attribute\n");
            var r = await context.Next();
            return r;
        }
    }
}
