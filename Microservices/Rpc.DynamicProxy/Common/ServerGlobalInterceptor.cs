﻿using System.Threading.Tasks;
using Common.Tools;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions;
using Rpc.DynamicProxy.Interceptor;

namespace Rpc.DynamicProxy.Common
{
    public class ServerGlobalInterceptor : InterceptorAbstract
    {
        //private ILogger Logger { get; }

        //public ServerGlobalInterceptor(ILogger<ServerGlobalInterceptor> logger)
        //{
        //    Logger = logger;
        //}


        public override async Task<IServiceResult> Intercept(IInterceptorContext context)
        {

            LogHelper.Info("\n---------------->Server global interceptor\n");
            return await context.Next();

        }
    }
}
