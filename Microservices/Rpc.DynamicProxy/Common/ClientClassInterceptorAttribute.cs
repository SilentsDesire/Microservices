﻿using System.Threading.Tasks;
using Common.Tools;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions;
using Rpc.DynamicProxy.Interceptor;

namespace Rpc.DynamicProxy.Common
{
    public class ClientClassInterceptorAttribute : InterceptorAttributeAbstract
    {
        //private ILogger Logger { get; }

        //public ClientClassInterceptorAttribute(ILogger<ServerInterceptorAttribute> logger)
        //{
        //    Logger = logger;
        //}

        public ClientClassInterceptorAttribute()
        {
        }

        public override async Task<IServiceResult> Intercept(IInterceptorContext context)
        {
            LogHelper.Info("\n------------------>Client class interceptor attribute\n");
            var r = await context.Next();
            return r;
        }
    }
}
