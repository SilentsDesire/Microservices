﻿using System.Threading.Tasks;
using Common.Tools;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions;
using Rpc.DynamicProxy.Interceptor;

namespace Rpc.DynamicProxy.Common
{
    public class ServerMethodInterceptorAttribute : InterceptorAttributeAbstract
    {
        //private ILogger Logger { get; }

        //public ServerMethodInterceptorAttribute(ILogger<ServerInterceptorAttribute> logger)
        //{
        //    Logger = logger;
        //}

        //public ServerMethodInterceptorAttribute()
        //{
        //}

        public override async Task<IServiceResult> Intercept(IInterceptorContext context)
        {
            LogHelper.Info("\n------------------>Server method interceptor attribute\n");
            var r = await context.Next();
            return r;
        }
    }
}
