﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Common.Tools;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions.CircuitBreaker;

namespace Rpc.DynamicProxy.Common
{
    public class CircuitBreakerEvent : ICircuitBreakerEvent
    {
        //private ILogger Logger { get; }

        //public CircuitBreakerEvent(ILogger<CircuitBreakerEvent> logger)
        //{
        //    Logger = logger;
        //}
        public async Task OnFallback(string route, MethodInfo methodInfo)
        {
            LogHelper.Warn("Raise OnFallback");
            await Task.CompletedTask;
        }

        public async Task OnBreak(string route, MethodInfo methodInfo, Exception exception, TimeSpan time)
        {
            LogHelper.Warn($"Raise OnBreak;{exception.Message}");
            await Task.CompletedTask;
        }

        public async Task OnRest(string route, MethodInfo methodInfo)
        {
            LogHelper.Warn("Raise OnRest");
            await Task.CompletedTask;
        }

        public async Task OnHalfOpen(string route, MethodInfo methodInfo)
        {
            LogHelper.Warn("Raise OnHalfOpen");
            await Task.CompletedTask;
        }

        public async Task OnTimeOut(string route, MethodInfo methodInfo, Exception exception)
        {
            LogHelper.Warn($"Raise OnTimeOut;{exception.Message}");
            await Task.CompletedTask;
        }

        public async Task OnRetry(string route, MethodInfo methodInfo, Exception exception, int retryTimes)
        {
            LogHelper.Warn($"Raise OnRetry;{exception.Message};{retryTimes}");
            await Task.CompletedTask;
        }

        public async Task OnBulkheadRejected(string route, MethodInfo methodInfo)
        {
            LogHelper.Warn("Raise OnBulkheadRejected;");
            await Task.CompletedTask;
        }
    }
}
