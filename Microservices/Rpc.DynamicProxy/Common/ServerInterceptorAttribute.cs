﻿using System.Threading.Tasks;
using Common.Tools;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions;
using Rpc.DynamicProxy.Interceptor;

namespace Rpc.DynamicProxy.Common
{
    public class ServerInterceptorAttribute : InterceptorAttributeAbstract
    {
        //private ILogger Logger { get; }

        //public ServerInterceptorAttribute(ILogger<ServerInterceptorAttribute> logger)
        //{
        //    Logger = logger;
        //}

        public ServerInterceptorAttribute()
        {
        }

        public override async Task<IServiceResult> Intercept(IInterceptorContext context)
        {
            LogHelper.Info("\n------------------>Server Interceptor attribute\n");
            var r = await context.Next();
            return r;
        }
    }
}
