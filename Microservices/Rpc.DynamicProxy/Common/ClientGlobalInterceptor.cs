﻿using System.Threading.Tasks;
using Common.Tools;
using Microsoft.Extensions.Logging;
using Rpc.Abstractions;
using Rpc.DynamicProxy.Interceptor;

namespace Rpc.DynamicProxy.Common
{
    public class ClientGlobalInterceptor : InterceptorAbstract
    {
        //private ILogger Logger { get; }

        //public ClientGlobalInterceptor(ILogger<ClientGlobalInterceptor> logger)
        //{
        //    Logger = logger;
        //}


        public override async Task<IServiceResult> Intercept(IInterceptorContext context)
        {

            LogHelper.Info("\n---------------->Client global interceptor\n");
            return await context.Next();

        }
    }
}
