﻿using System.Threading.Tasks;
using Rpc.Abstractions;


namespace Rpc.DynamicProxy.Interceptor
{
    public abstract class InterceptorAbstract : IInterceptor
    {
        public abstract Task<IServiceResult> Intercept(IInterceptorContext context);
    }
}
